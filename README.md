Virtual Programming lab for Moodle - Execution server running on Docker.

Original project: http://vpl.dis.ulpgc.es/

### Quick Start

```shell
docker run -it --env FQDN=host.domain.com --cap-add=SYS_ADMIN \ 
-p 80:80 -p 443:443 ifrscanoas/vpl-jail:2.5.2
```

### Build your own image

Clone this project, edit files as you need and build

#### Build

```shell
docker build -t TAG .
```

#### Run

```shell
docker run --env FQDN=host.domain.com --cap-add=SYS_ADMIN -it -p 80:80 -p 443:443 TAG
```

#### Development softwares installed

* C compiler (GNU)
* General purpose debugger (GNU)
* PHP interpreter
* Python interpreter


#### Info
FQDN: variable for generating self-signed SSL certificates.

--cap-add=SYS_ADMIN: without this parameter docker can't create the internal root jail for the executions.

#### Image
https://hub.docker.com/r/ifrscanoas/vpl-jail/
